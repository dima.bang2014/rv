package lab_2_3;

import static java.lang.System.out;

class Student
{
	//instance field
	String firstName;
	String lastName;
	String group;

	double averageMark;
	double cash;

	//constructors
	Student(String fName, String lName, String grp, double avr)
	{
		this.firstName = fName;
		this.lastName = lName;
		this.group = grp;
		this.averageMark = avr;
	}

	//methods
	public double getScholarship()
	{
		if(averageMark == 5)
			this.cash = 100;
		else this.cash = 80;

		return this.cash;
	}

	public void getInfo()
	{
		out.println("first name: " + this.firstName + "\n second name: " + this.lastName + "\n group: " + this.group + "\n cash: " + this.getScholarship());
	}
}

class Aspirant extends Student
{
	//instance field
	String scienceWork;

	//constructors
	Aspirant(String fn, String ln, String gp, double ae)
	{
		super(fn, ln, gp, ae);
	}

	//methods
	public double getScholarship()
	{
		if(averageMark == 5)
			this.cash = 200;
		else this.cash = 180;

		return this.cash;
	}
}

public class prog2_3
{
	public static void main(String args[])
	{
		Student newStudent = new Aspirant("12", "23", "34", 3);
		Student[] studentArray = new Student[3];

		studentArray[0] = new Student("Serge", "Panin", "BUT1701", 4);
		studentArray[1] = new Student("Vasiliy", "Pakhomov", "MPEI1702", 5);
		studentArray[2] = new Aspirant("Carl", "Johnson", "CYF2001", 5);

		for(Student e : studentArray)
			e.getInfo();
	}
}
