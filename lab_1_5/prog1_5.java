package lab_1_5;

import java.util.Scanner;

public class prog1_5
{

	public static int read()
	{
		int variable = 0;

		Scanner in = new Scanner(System.in);
		variable = in.nextInt();
		//in.close();

		return variable;
	}

	public static void text(String str1)//print in console
	{
		System.out.print(str1);
	}

        public static void textln()//print in console
        {
                System.out.print("");
        }

	public static int max(int[] array1)
	{

		int max = array1[0];

		for(int i = 1; i < array1.length; i++)
		if(max < array1[i])
			max = array1[i];

		return max;
	}

	public static void main(String args[])
	{
		int[] userArray = {0, 0, 0};
		int max = 0;

		for(int i = 0; i < 3; i++)
		{
			text("enter " + (i + 1) + " number: ");
			userArray[i] = read();
		}

		max = max(userArray);
		textln();
                text("Max is: " + max);

	}
}
